describe('Tabber and Tabs construction', function () {
	it('should have Tabber class defined', function () {
		expect(Tabber).toBeDefined();
	});
	it('should have Tab class defined', function () {
		expect(Tab).toBeDefined();
	});
	it('should have TabText class defined', function () {
		expect(TabText).toBeDefined();
	});
	it('should have TabSwitch class defined', function () {
		expect(TabSwitch).toBeDefined();
	});
});

describe('Tabber actions', function () {
	var tabber,
		tab;
		
	beforeEach(function () {
		var testNode = document.createElement('div');
		tabber = new Tabber(testNode);
		tab = new Tab();
	});
	it('should throw error when constructed without rootNode', function () {
		expect(function () {
			var wrongTabber = new Tabber();
		}).toThrow();
	});
	it('should contain no tabs after creation', function () {
		expect(tabber.tabs.length).toBe(0);
	});
	it('should allow to add instances of Tab class', function () {
		expect(tabber.add(tab)).toBeTruthy();
	});
	it('should disallow to add non Tab class to be added', function () {
		var nonTab = {};
		expect(tabber.add(nonTab)).toBe(false);
	});
	it('should allow to remove tab', function () {
		tabber.add(tab);
		expect(tabber.tabs.length).toBe(1);
		tabber.remove(0);
		expect(tabber.tabs.length).toBe(0);
	});
});