/**
 * Tabber
 *  Main Tabber class responsible for managing tabs
 */
var Tabber = function (rootNode) {
	var config = {
		menuElement       : 'ol',
		menuItemElement   : 'li',
		viewElement       : 'li',
		defaultName       : 'unnamed',
		activeClassName   : 'active',
		menuClassName     : 'Tabber-menu',
		menuItemClassName : 'Tabber-menu-item',
		viewClassName     : 'Tabber-view'
		
	};
	var that = this;
	var classReplaceRegex = new RegExp('(?:^|\\s)' + config.activeClassName + '(?!\\S)');
	var views = [];
	var items = [];
	var rootNode = rootNode;
	var menuNode = null;
	
	/**
	 * Array of Tab instances 
	 */
	this.tabs = [];
	
	if (!rootNode) {
		/* break hard and soon */
		throw 'Root node not provided';
	}
	
	/**
	 * Adds tab instance to Tabber
	 * Creates menu item and tab view
	 */
	this.add = function (tab) {
		if (tab instanceof Tab) {
			createTabView(tab);
			return true;
		}
		return false;
	};
	
	/**
	 * Removes tab (menu item and view)
	 * This version supports removing by index
	 */
	this.remove = function (index) {
		menuNode.removeChild(items[index]);
		rootNode.removeChild(views[index]);
		this.tabs.splice(index, 1);
		views.splice(index, 1);
		items.splice(index, 1);
	};
	
	/**
	 * Sets active tab along with updating menu item
	 */
	this.setActiveTab = function (index) {
		/* Remove `active` class from views and menu items */
		for (var i = 0, len = views.length; i<len; i++) {
			var viewCls = views[i].className;
			views[i].className = viewCls.replace(classReplaceRegex , '');
			
			var itemCls = items[i].className;
			items[i].className = itemCls.replace(classReplaceRegex , '');
		}
		
		views[index].className += ' ' + config.activeClassName;
		items[index].className += ' ' + config.activeClassName;
		this.activeTab = index;				
	};
	
	var addView = function (tab) {
		var view = document.createElement(config.viewElement);
		var content  = tab.getContent();
		
		if (typeof content === 'string') { 
			view.innerHTML = tab.getContent();
		} else if (content instanceof HTMLElement) {
			view.appendChild(content);
		}
		
		view.className = config.viewClassName;
		views.push(view);
		rootNode.appendChild(view);
	};
	
	var addMenuItem = function (tab) {
		var index = that.tabs.length;
		var label = tab.label || config.defaultName;
		var item = document.createElement(config.menuItemElement);
		
		that.tabs.push(tab);
		item.innerHTML = label;
		item.className = config.menuItemClassName;
		item.addEventListener('click', getEvent(index));
		items.push(item);
		menuNode.appendChild(item);
	};
	
	var createTabView = function (tab) {
		addMenuItem(tab);
		addView(tab);
		
		/* Set first tab as active */
		if (that.tabs.length === 1) {
			that.setActiveTab(0);
		}
	};
	
	var createMenu = function () {
		menuNode = document.createElement(config.menuElement);
		menuNode.className = config.menuClassName;
		rootNode.appendChild(menuNode);
	};
	
	var getEvent = function (index) {
		return (function onMenuItemClick() {
			this.setActiveTab(index);
		}).bind(that);
	};
	
	/**
	 * Init
	 */
	createMenu();
	
	/* Public */
	return {
		tabs         : this.tabs, /* just for tests */
		add          : this.add,
		remove       : this.remove,
		setActiveTab : this.setActiveTab
	}
};

/**
 * Generic tab
 */
var Tab = function (label) {
	this.label = label;
};
Tab.prototype.getContent = function () { };

/**
 * Tab for storing text
 */
var TabText = function (label) {
	Tab.call(this, label);
};
TabText.prototype = new Tab();
TabText.prototype.getContent = function () {
	return [
	'<h1>Quick brown fox jumps over the lazy dog</h1>',
	'<h2>Quick brown fox jumps over the lazy dog</h2>',
	'<h3>Quick brown fox jumps over the lazy dog</h3>',
	'<h4>Quick brown fox jumps over the lazy dog</h4>'].join('');
};

/**
 * Tab for switch
 */
var TabSwitch = function (label) {
	Tab.call(this, label);
};
TabSwitch.prototype = new Tab();
TabSwitch.prototype.getContent = function () {
	var rootElement = document.createElement('div');
	var switcher;
	
	rootElement.className = 'Switcher';
	switcher = new Switcher(rootElement);
	return rootElement;
};