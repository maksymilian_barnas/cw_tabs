/**
 * Switcher
 *  Simple component for logging on/off state to console
 */
var Switcher = function (rootNode) {
	var config = {
		spanLeftClassName  : 'Switcher-left',
		spanRightClassName : 'Switcher-right',
		isOnClassName      : 'Switcher--isChecked'
	};
	
	var isOnRegExp = new RegExp('(?:^|\\s)' + config.isOnClassName + '(?!\\S)');
	var rootNode = rootNode;
	var input, spanLeft, spanRight;
	
	if (!rootNode) {
		/* break hard and soon */
		throw 'Root node not provided';
	}
	
	var updateState = function ()  {
		console.log(input.checked ? 'on' : 'off');
		rootNode.className = rootNode.className.replace(isOnRegExp, '');
		if (input.checked) {
			rootNode.className += ' ' + config.isOnClassName;
		}
	};
	
	spanLeft = document.createElement('span');
	spanLeft.className = config.spanLeftClassName;
	rootNode.appendChild(spanLeft);
	
	spanRight = document.createElement('span');
	spanRight.className = config.spanRightClassName;
	rootNode.appendChild(spanRight);
	
	input = document.createElement('input');
	input.type = 'checkbox';
	
	input.addEventListener('change', (function () {
		return function () {
			updateState();
		};
	})());
	
	rootNode.appendChild(input);	
};