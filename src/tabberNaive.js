(function ($){

	var TabberNaive = function construct(rootId) {
		var config = {
			defaultName: 'Unnamed',
			defaultTab: 0
		};
		var rootElem = $(rootId);
		var activeTab = null;
		var menu = null;
		var tabs = null;
		var menuItems = [];		
		
		function createMenu() {
			menu = $('<ol>');
			tabs = collectTabs();
			
			for(var i = 0, len = tabs.length; i<len; i++) {
				var element = tabs[i];
				var label = element.dataset.label || config.defaultName;
				menu.append(createMenuItem(i, label));
			}
						
			menuItems = menu.children('li');
			rootElem.prepend(menu);
		}
		
		function collectTabs() {
			return rootElem.children('li') || [];
		}
		
		function createMenuItem(index, label) {
			return '<li data-index="' + index + '">' + label + '</li>';
		}
		
		function setActiveTab(index) {
			menu.children('li').removeClass('active');
			$(menuItems[index]).addClass('active');
			tabs.removeClass('active');
			$(tabs[index]).addClass('active');
			
			activeTab = index;				
		}
		
		function getEvent(index) {
			return function onMenuItemClick() {
				setActiveTab(index);
			}				
		}
		
		function init() {
			createMenu();
			
			if (tabs.length) {
				setActiveTab(config.defaultTab);
			}
			
			for (var i = 0, len = menuItems.length; i < len; i++) {
				var index = menuItems[i].dataset.index;
				menuItems[i].addEventListener('click', getEvent(index));
			}
			
		}
		
		function addTab() {
		}
		
		function removeTab() {
		}
		
		init();
	};
	
	window.Tabber = Tabber;
	
})(jQuery);